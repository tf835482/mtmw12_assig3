# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 17:05:41 2019

@author: Shammi Akhter
"""

import numpy as np

# Functions for calculating gradient

def gradient_2point(f, dx):
    "The gradient of array f assuming points are a distance dx apart"
    "using 2 point differences"
    
    # Initialised the array for the gradient to be the same size as f. As we are considering half our array will be twice of half
    dfdx = np.zeros_like(f*2 - 1)
    # Two point differences at the end point
    dfdx[0] = (f[1] - f[0]) / dx
    dfdx[-1] = (f[-1] - f[-2]) / dx
    # Centered differences for te mid-points
    for i in range(3,len(dfdx)-2):
        j = int(i//2)
        dfdx[i-1] = (f[j+1] - f[j-1]) / (2*dx) 
        #rules for half
        dfdx[i] = (9*f[j] + 9*f[j+1] - f[j-1] - f[j+2]) / (16*dx)
    return dfdx
    
