# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 17:17:15 2019

@author: Shammi Akhter
"""

# MTMW12 assignment 3.
''' Python3 code to numerically differentiate the pressure in order to calculate geostrophic wind relation using 2-point differencing, compare with the analytic solution and polt''' 

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *

def geostrophicWind():
    # Input parameters describing the problem
    import geoParameters as gp
    
    #Resolution
    N = 10            # the number of intervals to divide space into
    dy = (gp.ymax - gp.ymin)/N    # the lengt of the spacing
    
    # The spatial dimension, y:
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    
    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    # The pressure at the y points
    p = gp.pressure(y)
    
    #The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = gp.geoWind(dpdy)
    
    # Graph to compare the numerical and analytic solutions
    #plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    #plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k--', label='Exact')
    plt.plot( y/1000 , u_2point, '*k--' , label='Two−point differences', \
ms=12, markeredgewidth = 1.5 , markerfacecolor = 'none' )
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCent.pdf')
    plt.show()
    
if __name__ == "__main__":
     geostrophicWind()


     