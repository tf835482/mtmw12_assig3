import unittest
import geoParameters as gp

class TestStringMethods(unittest.TestCase):

    def test_uExact(self):
        self.assertEqual(round(gp.uExact(100000.),8), 0.34171677 )
        self.assertEqual(round(gp.uExact(900000.),8), 2.41871096 )
    
    def test_pressure(self):
        self.assertEqual(round(gp.pressure(1.00186288),8), 100184.7759065 )
        self.assertEqual(round(gp.pressure(1.30899694),8),  100173.20508076 )

if __name__ == '__main__':
    unittest.main()